/**
* @file	    list_dup_enc_sent.h
* @brief	Arquivo com a classe List, seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	11/04/2017
* @date     13/04/2017
*/

#ifndef LIST_DUP_ENC_SENT
#define LIST_DUP_ENC_SENT

#include "node.h"

/**
* @class  List list_dup_enc_sent.h
* @brief  Classe List e seus atributos.
*/
template <typename T>
class List {
    
    private:

    Node<T> *start;
    Node<T> *end;

    public:

    List();

    void insertElement (T element);
    void printElements ();
    void removeElement (T element);

    ~List();

};

/**
* @brief Contrutor para a classe List.
*/
template <typename T>
List<T>::List () {
    
    start = NULL;
    end = NULL;
}

/**
* @brief  Método que coloca um elemento na lista.
* @param  element Variavél que recebe o elemento a ser colocado na lista.
*/
template <typename T>
void List<T>::insertElement (T element) {

    Node <T> *New = new Node <T>;
    New->setElement (element);

    if (start == NULL) {
        start = New;
        end = New;
    }
    else {
        Node <T> *aux;
        aux = start;

        while (aux != NULL) {
            if (element > aux->getElement()) {
                aux = aux->getNext();
            }
            else { // primeira posição
                if (aux->getPrev() == NULL) {
                    New->setNext(aux);
                    aux->setPrev(New);
                    start = New;
                    return;
                }
                else {// qualquer lugar
                    New->setNext(aux);
                    New->setPrev(aux->getPrev());
                    aux->setPrev(New);
                    New->getPrev()->setNext(New);
                    return;
                }
            }
        }

        end->setNext(New);
        New->setPrev(end);
        end = New;
    }
}

/**
* @brief  Método que imprime os elementos que estao na lista.
*/
template <typename T>
void List<T>::printElements () {
    Node <T> *aux = start;
    while (aux != NULL) {
        cout << aux->getElement() << endl;
        aux = aux->getNext();
    }
}

/**
* @brief  Método que remove um elemento da lista.
* @param  element Variavél que recebe o elemento a ser removido da lista.
*/
template <typename T>
void List<T>::removeElement (T element) {

    Node <T> *aux = start;    

    if (aux == NULL) {
        cout << "Empty list!" << endl;
        return;
    }

    if (aux->getElement() == element) { // remove o primeiro 
        
        if (start->getNext()) {
            aux = start->getNext();
            aux->setPrev(NULL);
            
            delete start;

            start = aux;

        } 
        else {

            delete start;

            start = NULL;
            end = NULL;
        }

        return;
    }

    aux = aux->getNext();
        
    while (aux != NULL) {
    
        if ( (aux->getElement() == element) ) {

            if (aux == end) { //remove o ultimo
                if (aux == start) {

                    start = NULL;
                    end = NULL;

                    start = end;

                    return;
                }
                else {

                    aux->getPrev()->setNext(NULL);
                    end = aux->getPrev();
                    
                    delete aux;
                    return;
                }
            }

            else { // remove o do meio
                Node <T> *temp = new Node <T>;
            
                temp->setNext( aux->getNext() );
                aux->setNext(temp);

                aux->getPrev()->setNext( temp->getNext() );

                temp->getNext()->setPrev( aux->getPrev() );
                aux->setPrev(temp);


                delete temp;
                delete aux;               
                return;
            }

        }
        
    aux = aux->getNext();
    
    }
    cout << "Element is not here!" << endl;     
}

/**
* @brief Destrutor padrão para liberar o espaço na memória.
*/
template <typename T>
List<T>::~List() {

    while (start && end) {
        
        Node <T> *temp = start;
        removeElement(temp->getElement());  
    }
}

#endif