/**
* @file	    node.h
* @brief	Arquivo com a classe Node, seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	11/04/2017
* @date     13/04/2017
*/

#ifndef NODE_H
#define NODE_H 

/**
* @class  Node node.h.
* @brief  Classe Node e seus atributos.
*/
template <typename T>
class Node {

    private:

    T element;
    Node *next;
    Node *prev;

    public:

    Node ();

    T getElement ();
    void setElement (T e);

    Node<T>* getNext ();
    void setNext (Node *n);

    Node<T>* getPrev ();
    void setPrev (Node *p);
};

/**
* @brief Contrutor para a classe Node.
*/
template <typename T>
Node<T>::Node () {
    
    next = NULL;
    prev = NULL;
}

/**
* @brief  Método que retorna o elemento.
* @return Retorna o elemento do nó.
*/
template <typename T>
T Node<T>::getElement () {
    return element;
}

/**
* @brief Método que atribui um elemento ao atributo da clase.
* @param e Variável que rebece o elemento a ser atribuido.
*/
template <typename T>
void Node<T>::setElement (T e) {
    element = e;
}

/**
* @brief  Método que retorna o próximo objeto do nó.
* @return Retorna o próximo objeto do nó.
*/
template <typename T>
Node<T>* Node<T>::getNext () {
    return next;
}

/**
* @brief Método que atribui o próximo nó ao atributo 'next' da clase.
* @param *n Ponteiro que recebe a referência do próximo nó
*/
template <typename T>
void Node<T>::setNext (Node *n) {
    next = n;
}

/**
* @brief Método que retorna o nó anterior do atual.
* @param Retorna o nó anterior.
*/
template <typename T>
Node<T>* Node<T>::getPrev () {
    return prev;
}

/**
* @brief Método que atribui o nó anterior do atual, para o atributo 'prev' da clase.
* @param *p Ponteiro que recebe a referência do nó anterior.
*/
template <typename T>
void Node<T>::setPrev (Node *p) {
    prev = p;
}

#endif