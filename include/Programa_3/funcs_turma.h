/**
* @file	    funcs_turma.h
* @brief	Arquivo com os cabeçários das as funções de cadastro de turmas e inclusão de alunos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     15/04/2017
*/

#ifndef FUNCS_TURMA_H_
#define FUNCS_TURMA_H_

/**
* @brief  Função que realiza o cadastro da turma.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  *qtd_turmas Ponteiro que recebe a referência da quantidade de turmas cadastradas.
*/
void cadastrar_turma (Turma *turma, int *qtd_turmas);

/**
* @brief  Função que realiza o cadastro dos alunos.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  indice_aux Variável que recebe a posição da turma especifica no vetor.
* @param  opcao Variável que recebe a opcao do usuário de cadastrar os alunos pelo prgrama ou por arquivo.
*/
int cadastrar_alunos ( Turma *turma, int indice_aux, int opcao);

/**
* @brief  Função que realiza a busca da turma.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  qtd_turmas Variável que recebe a quantidade de turmas cadastradas.
*/
int busca_turma (Turma *turmas, int num_turmas);

/**
* @brief Função que carrega um arquivo com turmas e seus dados.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  &qtd_turmas Referência a quantidade de turmas cadastradas.
*/
void salvar_turmas (Turma *turmas, int qtd_turmas, int indice_aux);

/**
* @brief Função que salva em um arquivo as turmas e os dados de seus alunos.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  qtd_turmas Variável que recebe a quantidade de turmas cadastradas.
* @param  indice_aux Variável que recebe a posição da turma especifica no vetor.
*/
void carregar_arquivo (Turma *turmas, int &num_turmas);

#endif