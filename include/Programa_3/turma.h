/**
* @file	    turma.h
* @brief	Arquivo com a classe turma, seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     15/04/2017
*/

#ifndef TURMA_H_
#define TURMA_H_

#include <string>
using std:: string;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include "aluno.h"
#include "list_dup_enc_sent_.h"

/**
* @class  Turma turma.h.
* @brief  Classe Turma e seus atributos.
*/
class Turma {

    private:

    string codigo;
    List <Aluno> alunos;
    int qtd_alunos;
    float media;
    float total_notas;

    public:

    /**
    * @brief Contrutor para a classe Turma.
    */
    Turma ();
    
    /**
    * @brief  Método que retorna o código da turma.
    * @return Retorna o código da turma.
    */
    string getCodigo ();

    /**
    * @brief  Método que atribui o código de um objeto do tipo Turma.
    * @param  n Varável que recebe o código a ser atribuido.
    */
    void setCodigo (string n);

    /**
    * @brief  Método que adiociona um aluno na turma.
    * @param  n Varável que recebe o nome do aluno.
    * @param  m Variável que recebe a matricula do aluno.
    * @param  f Variável que recebe as faltas do aluno.
    * @param  nota Variável que recebe a nota do aluno.
    * @param  *alunos_cadastrados Ponteiro que recebe a referencia da quantidade de alunos já cadastrados.
    */
    void add_aluno(string n, string m, string f, string nota, int *alunos_cadastrados);

    /**
    * @details Metodo que remove um aluno da lista de alunos e recalcula a media da turma.
    */
    void Remover_aluno ();

    /** 
    * @details  Método que gera para a saída padrão os dados dos alunos.
    * @param	saida Arquivo para gravação dos dados.
    */
    void Salvar_arquivo (ofstream &saida);

    /** 
    * @details Metodo que carrega os dados de uma turma de um arquivo.
    * @param	arquivo Arquivo para leitura dos dados.
    */
    void Carregar_arquivo (ifstream &arquivo);

    /** 
    * @details  Operador "<<" sobrecarregado para gerar os dados dos alunos na saida padrão.
    * @param	os Referencia para stream de saida.
    * @param	t Referencia para o objeto Turma a ser impresso
    * @return	Referencia para stream de saida
    */
    friend ostream& operator << (ostream &os, Turma &t);

    /** 
    * @details Operador "=" sobrecarregado para realizar uma atribuição de um objeto a outro.
    * @param	t Referencia para o objeto Turma que invoca o metodo.
    * @return	Autoreferência para o objeto que invoca o metodo, com atributos atualizados.
    */
    Turma& operator = (Turma const &t);

};

#endif