/**
* @file	    list_dup_enc_sent_.h
* @brief	Arquivo com a classe List, seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	11/04/2017
* @date     13/04/2017
*/

#ifndef LIST_DUP_ENC_SENT_H
#define LIST_DUP_ENC_SENT_H

#include <string>
using std:: string; 

#include "node.h"

/**
* @class  List list_dup_enc_sent_.h.
* @brief  Classe List e seus atributos.
*/
template <typename T>
class List {
    
    private:

    Node<T> *start;
    Node<T> *end;

    public:

    List();
    
    void insertElement (T element);
    void printElements ();
    void removeElement (T element);
    bool findElement (T element);
    string ElementToString ();
    T findElement2 (T element);

    ~List();

};

/**
* @brief Contrutor para a classe List.
*/
template <typename T>
List<T>::List () {
    
    start = NULL;
    end = NULL;
}

/**
* @brief  Método que coloca um elemento na lista.
* @param  element Variavél que recebe o elemento a ser colocado na lista.
*/
template <typename T>
void List<T>::insertElement (T element) {

    Node <T> *New = new Node <T>;
    New->setElement (element);

    if (start == NULL) {
        start = New;
        end = New;
    }
    else {
        Node <T> *aux;
        aux = start;

        while (aux != NULL) {
            if (element > aux->getElement()) {
                aux = aux->getNext();
            }
            else { // primeira posição
                if (aux->getPrev() == NULL) {
                    New->setNext(aux);
                    aux->setPrev(New);
                    start = New;
                    return;
                }
                else {// qualquer lugar
                    New->setNext(aux);
                    New->setPrev(aux->getPrev());
                    aux->setPrev(New);
                    New->getPrev()->setNext(New);
                    return;
                }
            }
        }

        end->setNext(New);
        New->setPrev(end);
        end = New;
    }
}

/**
* @brief  Método que imprime os elementos que estao na lista.
*/
template <typename T>
void List<T>::printElements () {
    Node <T> *aux = start;
    while (aux != NULL) {
        T temp = aux->getElement();
        cout << temp << endl;
        aux = aux->getNext();
    }
}

/**
* @brief  Método que remove um elemento da lista.
* @param  element Variavél que recebe o elemento a ser removido da lista.
*/
template <typename T>
void List<T>::removeElement (T element) {

    Node <T> *aux = start;    

    if (aux == NULL) {
        cout << "Lista vazia!" << endl;
        return;
    }

    if (aux->getElement() == element) { // remove o primeiro 
        
        if (start->getNext()) {
            aux = start->getNext();
            aux->setPrev(NULL);
            
            delete start;

            start = aux;

        } 
        else {

            delete start;

            start = NULL;
            end = NULL;
        }

        return;
    }

    aux = aux->getNext();
        
    while (aux != NULL) {
    
        if ( (aux->getElement() == element) ) {

            if (aux == end) { //remove o ultimo
                if (aux == start) {

                    start = NULL;
                    end = NULL;

                    start = end;

                    return;
                }
                else {

                    aux->getPrev()->setNext(NULL);
                    end = aux->getPrev();
                    
                    delete aux;
                    return;
                }
            }

            else { // remove o do meio
                Node <T> *temp = new Node <T>;
            
                temp->setNext( aux->getNext() );
                aux->setNext(temp);

                aux->getPrev()->setNext( temp->getNext() );

                temp->getNext()->setPrev( aux->getPrev() );
                aux->setPrev(temp);


                delete temp;
                delete aux;               
                return;
            }

        }
        
    aux = aux->getNext();
    
    }
}

/**
* @brief  Metodo que realiza uam busca para encontrar um específico objeto da lista.
* @param  element Variável que recebe o elemento a ser buscado.
* @return Retorna um booleano, se encontrou o elemento ou não.
*/
template < typename T >
bool List<T>::findElement (T element) {

    Node <T> *temp = start;

    while (temp != NULL) {
        if (temp->getElement () == element) {
            return true;            
        }
        else {
            temp = temp->getNext ();
        }
    }

    delete [] temp;
    return false;
}

/**
* @brief  Metodo que realiza uam busca para encontrar um específico objeto da lista.
* @param  element Variavél que recebe o elemento a ser buscado.
* @return Retorna 
*/
template < typename T >
T List<T>::findElement2 (T element) {

    Node <T> *temp = start;

    while (temp != NULL) {
        if (temp->getElement () == element) {
            return temp->getElement();            
        }
        else {
            temp = temp->getNext ();
        }
    }

    delete [] temp;
    return element;
}

/**
* @brief  Metodo que realiza a concartenação dos elementos em uma string para a impressão dos dados.
* @return Retorna a string com todos os dados da lista.
*/
template <typename T>
string List<T>::ElementToString () {

    Node <T> *aux = start;

    string element;

    if (start != NULL) {
        while(aux) {
            element += aux->getElement ().dados_para_arquivo ();
            aux = aux->getNext();
        }
    }

    return element;
}

/**
* @brief Destrutor padrão para liberar o espaço na memória.
*/
template <typename T>
List<T>::~List() {

    while (start && end) {
        
        Node <T> *temp = start;
        removeElement(temp->getElement());  
    }
}

#endif