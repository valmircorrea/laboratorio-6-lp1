/**
* @file	    aluno.h
* @brief	Arquivo com a classe Aluno e seus metodos..
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     20/04/2017
* @sa       aluno.h
*/

#ifndef ALUNO_H_
#define ALUNO_H_

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
* @class  Aluno aluno.h.
* @brief  Classe Aluno e seus atributos.
*/
class Aluno {

    private:

    string nome;
    int matricula;
    int faltas;
    int nota;

    public:

    /**
    * @brief Contrutor para a classe Aluno.
    */
    Aluno();

    /**
    * @brief  Método que retorna a matricula do aluno.
    */
    int getMatricula ();

    /**
    * @brief  Método que atribui a matricula do aluno.
    * @param  m Variavél que recebe a matricula a ser colocada.
    */
    void setMatricula (int m);

    /**
    * @brief  Método que retorna o nome do aluno.
    */
    string getNome ();

    /**
    * @brief  Método que atribui o nome do aluno.
    * @param  n Variavél que recebe o nome a ser colocado.
    */
    void setNome (string n);

    /**
    * @brief  Método que retorna as faltas do aluno.
    */
    int getFaltas ();

    /**
    * @brief  Método que atribui a quantidade faltas do aluno.
    * @param  f Variavél que recebe a quantidade de faltas a ser colocado.
    */
    void setFaltas (int f);

    /**
    * @brief  Método que retorna a nota do aluno.
    */
    int getNota ();
    
    /**
    * @brief  Método que atribui a nota do aluno.
    * @param  n Variavél que recebe a nota a ser colocada.
    */
    void setNota (int n);

    /** 
    * @details Metodo atribui os dados do aluno em uma string para auxiliar na saida dos dados para o arquivo.
    * @return	String com os dados do aluno
    */
    string dados_para_arquivo ();

    /** 
    * @details  Operador "==" sobrecarregado para comparar dois alunos.
    * @param	a Variavél que recebe um objeto Aluno. 
    * @return	Verdadeiro ou Falso, dependendo da igualdade entre os dados dos alunos.
    */
    bool operator== (Aluno const a);

    /** 
    * @details  Operador ">" sobrecarregado para verificar a ordenação de cada aluno.
    * @param	a Variavél que recebe um objeto Aluno. 
    * @return	Verdadeiro ou Falso, dependendo da ordenação e entre os dados dos alunos.
    */
    bool operator > (Aluno const a);

    /** 
    * @details  Operador "<<" sobrecarregado para gerar os dados dos alunos na saida padrão.
    * @param	&os Referência para stream de saida.
    * @param	&a Referência para o objeto 'Aluno' a ser impresso.
    * @return	Referência para stream de saida.
    */
    friend ostream& operator<< (ostream &os, Aluno &a);

};

#endif