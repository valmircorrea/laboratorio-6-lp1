/**
* @file	    menus.cpp
* @brief	Arquivo com os cabeçários das funções de menu.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     15/04/2017
*/

#ifndef MENUS_H_
#define MENUS_H_

/**
* @brief Função que apresenta o menu principal.
* @return Opcao escolhida pelo usuario.
*/
int menu_principal ();

/**
* @brief Função que apresenta o menu específico para a inclusão de alunos.
* @return Opcao escolhida pelo usuário.
*/
int menu_aluno ();

#endif