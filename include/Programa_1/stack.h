/**
* @file	    stack.h
* @brief	Arquivo com a classe Stack, seus objetos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	09/04/2017
* @date     11/04/2017
*/

#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;

#ifndef STACK_H
#define STACK_H

/**
* @class   Stack stack.h
* @brief   Classe que representa o objeto Stack;
*/
template <typename T>
class Stack {
    private:

    T *vector;       /** <Vetor */
    int n_elements;  /** <Numero de elementos */
    int size_max;    /** <Tamanho máximo */ 

    public:

    Stack(int n);

    void push (T element);   /** <Insere no topo */
    void pop ();             /** <Remove */
    T top ();                /** <Retornar o ultimo caractere do vetor */
    bool full ();            /** <Vetifica se está cheio */

    ~Stack();
};

/**
* @brief Contrutor para a classe Stack.
*/
template <typename T>
Stack<T>::Stack (int n) {

    vector = new T [n];
    n_elements = 0;
    size_max = n;
}

/**
* @brief  Método que coloca um elemento na pilha.
* @param  element Variavél que recebe o elemento a ser colocado na pilha.
*/
template <typename T>
void Stack<T>::push (T element) {

    if ( !full () ) {

        vector[n_elements] = element;
        n_elements++;
    }

    else {
        cout << "Stack is full!" << endl;
    }
}

/**
* @brief  Método que tira um elemento no final da pilha.
*/
template <typename T>
void Stack<T>::pop () {

    n_elements--;
}

/**
* @brief Método que acessa o ultimo elemento da pilha.
* @return Retorna o ultimo elemento da pilha.
*/
template <typename T>
T Stack<T>::top () {
    return vector[n_elements-1];
}

/**
* @brief Método que verifica se a pilha esta cheia.
* @return Retorna 'true' se estiver cheio ou 'false', se não ésta cheio.
*/
template <typename T>
bool Stack<T>::full () {
    
    return (n_elements == size_max); 
}

/**
* @brief Destrutor padrão para liberar o espaço na memória.
*/
template <typename T>
Stack<T>::~Stack () {

    delete [] vector;
}

#endif