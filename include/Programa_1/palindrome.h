/**
* @file	    palindrome.h
* @brief	Arquivo com a função que organiza a frase ou palavra.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	09/04/2017
* @date     11/04/2017
*/

#ifndef PALINDROME_H_
#define PALINDROME_H_

/**
* @brief  Função que verifica se a palavra é um palíndromo.
* @param  word Variável que recebe a palavra que o usuário digitou.
* @param *letters Ponteiro que aponta para um endereço de uma classe do tipo Stack. 
* @param  size Tamanho da palavra.
*/
void palindrome ( string half, Stack <char> *letters, unsigned int size );

#endif