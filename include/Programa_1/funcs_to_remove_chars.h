/**
* @file	    funcs_to_remove_chars.h
* @brief	Arquivo com os cabeçários da funções que tiram os caracteres especiais da string.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	09/04/2017
* @date     11/04/2017
*/

#ifndef FUNCS_TO_REMOVE_CHARS_H_
#define FUNCS_TO_REMOVE_CHARS_H_

/**
* @brief  Função que deixa todas a letras minúsculas.
* @param  new_word Variável que recebe a nova string.
* @param  word Variável que recebe a palavra digitada pelo usuário. 
* @param  size Tamanho da palavra.
*/
string to_lower (string new_word, string word, unsigned int size);

/**
* @brief  Função que tira todos os caracteres especieais e os espaços.
* @param  new_word Variável que recebe a nova string. 
* @param  *size Tamanho da palavra.
*/
string remove_chars (string new_word, unsigned int *size);

/**
* @brief  Função que tira todos os espaços especificos que ficaram na palavra.
* @param  new_word Variável que recebe a nova string. 
* @param  *size Tamanho da palavra.
*/
string remove_space (string new_word, unsigned int *size);

/**
* @brief  Função que tira todos os acentos das vogais.
* @param  new_word Variável que recebe a nova string. 
* @param  *size Tamanho da palavra.
*/
string remove_especial_chars (string new_word, unsigned int *size);

#endif