/**
* @file	    remove_chars.h
* @brief	Arquivo com o cabeçário da função que tira os caracteres especiais da string.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	09/04/2017
* @date     11/04/2017
*/

#ifndef REMOVE_CHARS_H_
#define REMOVE_CHARS_H_

/**
* @brief  Tira os caracteres especiais e adapta a string para a comparação.
* @param  word Variável que recebe a palavra que o usuário digitou.
* @param  size Tamanho da palavra.
*/
string CleanSpecialChars (string word, unsigned int *size);

#endif