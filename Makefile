#Makefile for "laboratorio-06" C++ application
#Created by Valmir Correa 09/05/2017

RM = rm -rf

# Compilador:
CC = g++

# Variaveis para os subdiretorios:

INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
DEF_DIR=./data/default

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++11

# Assegura que os alvos não sejam confundidos com os arquivos de mesmo nome:
.PHONY: all clean distclean doxy

# Define o alvo para a compilação completa:
all: Programa_1 Programa_2 Programa_3

# Define o alvo para a compilação em partes:
Programa_1: Programa_1
Programa_2: Programa_2
Programa_3: Programa_3

# Debug:
debug: CFLAGS += -g -O0
debug: clean Programa_1 Programa_2 Programa_3


# Alvo para a contrução do executável Programa_1:
Programa_1: CFLAGS += -I. -I$(INC_DIR)/Programa_1
Programa_1: $(OBJ_DIR)/main.o $(OBJ_DIR)/palindrome.o $(OBJ_DIR)/remove_chars.o $(OBJ_DIR)/funcs_to_remove_chars.o 
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main.o:
$(OBJ_DIR)/main.o: $(SRC_DIR)/Programa_1/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do palindrome.o:
$(OBJ_DIR)/palindrome.o: $(SRC_DIR)/Programa_1/palindrome.cpp $(INC_DIR)/Programa_1/palindrome.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do remocao_carac.o:
$(OBJ_DIR)/remove_chars.o: $(SRC_DIR)/Programa_1/remove_chars.cpp $(INC_DIR)/Programa_1/remove_chars.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do funcs_to_remove_carac.o:	
$(OBJ_DIR)/funcs_to_remove_chars.o: $(SRC_DIR)/Programa_1/funcs_to_remove_chars.cpp $(INC_DIR)/Programa_1/funcs_to_remove_chars.h
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo para a contrução do executável Programa_2:
Programa_2: CFLAGS += -I. -I$(INC_DIR)/Programa_2
Programa_2: $(OBJ_DIR)/main2.o 
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main2.o:
$(OBJ_DIR)/main2.o: $(SRC_DIR)/Programa_2/main2.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo para a contrução do executável Programa_3:
Programa_3: CFLAGS += -I. -I$(INC_DIR)/Programa_3
Programa_3: $(OBJ_DIR)/main3.o $(OBJ_DIR)/aluno.o $(OBJ_DIR)/turma.o $(OBJ_DIR)/menus.o $(OBJ_DIR)/funcs_turma.o
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main2.o:
$(OBJ_DIR)/main3.o: $(SRC_DIR)/Programa_3/main3.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução do aluno.o:
$(OBJ_DIR)/aluno.o: $(SRC_DIR)/Programa_3/aluno.cpp $(INC_DIR)/Programa_3/aluno.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução do turma.o:
$(OBJ_DIR)/turma.o: $(SRC_DIR)/Programa_3/turma.cpp $(INC_DIR)/Programa_3/turma.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução do menus.o:
$(OBJ_DIR)/menus.o: $(SRC_DIR)/Programa_3/menus.cpp $(INC_DIR)/Programa_3/menus.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução do funcs_turma.o:
$(OBJ_DIR)/funcs_turma.o: $(SRC_DIR)/Programa_3/funcs_turma.cpp $(INC_DIR)/Programa_3/funcs_turma.h
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes


# Alvo para a geração automatica de documentação usando o Doxygen:
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile
doxyg:
	doxygen -g


# Alvo usado para limpar os arquivos temporarios:
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
	$(RM) $(DEF_DIR)/*
