/**
* @file	    main2.cpp
* @brief	Arquivo com a função principal.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	11/04/2017
* @date     14/04/2017
*/

#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;

#include "node.h"
#include "list_dup_enc_sent.h"

/**
* @brief função principal.
*/
int main () {

    List<int> ListaDuplEncadeada;

    cout << "Os elementos abaixo seram incializados apenas para testes da implementação da lista!" << endl;

    ListaDuplEncadeada.insertElement(10);
    ListaDuplEncadeada.insertElement(15);
    ListaDuplEncadeada.insertElement(8);
    ListaDuplEncadeada.insertElement(13);

    ListaDuplEncadeada.printElements();

    int num;
    cout << "Enter the number that you want to remove: ";
    cin >> num;

    ListaDuplEncadeada.removeElement(num);
    ListaDuplEncadeada.printElements();

    return 0;
}