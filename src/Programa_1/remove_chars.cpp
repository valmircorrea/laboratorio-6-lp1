/**
* @file	    remove_chars.h
* @brief	Arquivo com a função que tira os caracteres especiais da string.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	09/04/2017
* @date     11/04/2017
*/

#include <iostream> 
using std:: cout;
using std:: endl;

#include <string>
using std:: string;

#include <cctype>

#include "remove_chars.h"
#include "funcs_to_remove_chars.h"

/**
* @brief  Tira os caracteres especiais e adapta a string para a comparação.
* @param  word Variável que recebe a palavra que o usuário digitou.
* @param  size Tamanho da palavra.
*/
string CleanSpecialChars (string word, unsigned int *size) {

    string new_word = "";

    new_word = to_lower (new_word, word, *size);
    new_word = remove_chars (new_word, &(*size));
    new_word = remove_especial_chars (new_word, &(*size));
    new_word = remove_space (new_word, &(*size));

    return new_word;
}