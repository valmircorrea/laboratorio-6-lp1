/**
* @file	    main.cpp
* @brief	Arquivo com a função principal.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	09/04/2017
* @date     11/04/2017
*/

#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;

#include <cstring>
using std::string;

#include "stack.h"
#include "remove_chars.h"
#include "palindrome.h"

/**
* @brief  Função princípal.
*/
int main () {

    string word;

    cout << endl << "Enter a string: ";
    getline(cin, word);

    unsigned int size = word.length();

    word = CleanSpecialChars (word, &size);

    Stack <char> letters(size);

    for (unsigned int ii = 0; ii < size; ii++) {
        char l = word[ii];
        letters.push(l);
    }

    palindrome(word, &letters, size);

    return 0;
}