/**
* @file	    funcs_to_remove_chars.cpp
* @brief	Arquivo com a função que tira os caracteres especiais da string.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	09/04/2017
* @date     11/04/2017
*/

#include <string>
using std::string;

#include <cctype>

#include "funcs_to_remove_chars.h"

/**
* @brief  Função que deixa todas a letras minúsculas.
* @param  new_word Variável que recebe a nova string.
* @param  word Variável que recebe a palavra digitada pelo usuário. 
* @param  size Tamanho da palavra.
*/
string to_lower (string new_word, string word, unsigned int size) {

    for (unsigned int ii = 0; ii < size; ii++) {
        new_word += tolower (word[ii]);
    }

    return new_word;
}

/**
* @brief  Função que tira todos os caracteres especieais e os espaços.
* @param  new_word Variável que recebe a nova string. 
* @param  *size Tamanho da palavra.
*/
string remove_chars (string new_word, unsigned int *size) {

    unsigned int cont_aux = *size;

    for (unsigned int ii = 0; ii < *size; ii++) {
        if (ispunct(new_word[ii]) || isspace(new_word[ii]) ) {
            new_word.erase(ii, 1);
            cont_aux -= 1;
        }
    }

    *size = cont_aux;

    return new_word;
}

/**
* @brief  Função que tira todos os espaços especificos que ficaram na palavra.
* @param  new_word Variável que recebe a nova string. 
* @param  *size Tamanho da palavra.
*/
string remove_space (string new_word, unsigned int *size) {

    unsigned int cont_aux = *size;

    for (unsigned int ii = 0; ii < *size; ii++) {
        if (isspace(new_word[ii]) ) {
            new_word.erase(ii, 1);
            cont_aux -= 1;
        }
    }

    *size = cont_aux;

    return new_word;
}

/**
* @brief  Função que tira todos os acentos das vogais.
* @param  new_word Variável que recebe a nova string. 
* @param  *size Tamanho da palavra.
*/
string remove_especial_chars (string new_word, unsigned int *size) {
    
    string SpecialCharsA = "áàâÁÀÂ";
    string SpecialCharsE = "éèêÉÈÊ";
    string SpecialCharsI = "íìîÍÌÎ";
    string SpecialCharsO = "óòôÓÒÔ";
    string SpecialCharsU = "úùûÚÙÛ";

    unsigned int cont = *size;
    string word_aux = "";
    
    for (unsigned int ii = 0; ii < *size; ii++) {

        for (int jj = 0; jj < 12; jj++) {

            if ( (new_word[ii] == SpecialCharsA[jj]) && (new_word[ii+1] == SpecialCharsA[jj+1]) ) {
                for (unsigned int pp = 0; pp < cont; pp++) {
                    if (pp != ii) {
                        word_aux += new_word[pp];
                    }
                    else {
                        word_aux += 'a';
                        pp++;
                    }
                 }

                new_word = word_aux;
                word_aux = "";
                cont--;
                break;
            }
            else if ( (new_word[ii] == SpecialCharsE[jj]) && (new_word[ii+1] == SpecialCharsE[jj+1]) ) {
                for (unsigned int pp = 0; pp < cont; pp++) {
                    if (pp != ii) {
                        word_aux += new_word[pp];
                    }
                    else {
                        word_aux += 'e';
                        pp++;
                    }
                 }

                new_word = word_aux;
                word_aux = "";
                cont--;
                break;
            }
            if ( (new_word[ii] == SpecialCharsI[jj]) && (new_word[ii+1] == SpecialCharsI[jj+1]) ) {
                for (unsigned int pp = 0; pp < cont; pp++) {
                    if (pp != ii) {
                        word_aux += new_word[pp];
                    }
                    else {
                        word_aux += 'i';
                        pp++;
                    }
                 }

                new_word = word_aux;
                word_aux = "";
                cont--;
                break;
            }
            if ( (new_word[ii] == SpecialCharsO[jj]) && (new_word[ii+1] == SpecialCharsO[jj+1]) ) {
                for (unsigned int pp = 0; pp < cont; pp++) {
                    if (pp != ii) {
                        word_aux += new_word[pp];
                    }
                    else {
                        word_aux += 'o';
                        pp++;
                    }
                 }

                new_word = word_aux;
                word_aux = "";
                cont--;
                break;
            }
            if ((new_word[ii] == SpecialCharsU[jj]) && (new_word[ii+1] == SpecialCharsU[jj+1]) ) {
                for (unsigned int pp = 0; pp < cont; pp++) {
                    if (pp != ii) {
                        word_aux += new_word[pp];
                    }
                    else {
                        word_aux += 'u';
                        pp++;
                    }
                 }

                new_word = word_aux;
                word_aux = "";
                cont--;
                break;
            }
        }
    }

    *size = cont;
    return new_word;
}