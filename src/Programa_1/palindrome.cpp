/**
* @file   palindrome.cpp
* @brief  Arquivo com a função palindromo.
* @author Valmir Correa
* @since  10/03/17
* @date   11/03/17
*/

#include <iostream> 
using std:: cout;
using std:: endl;

#include <cstring>
using std::string;

#include "stack.h"
#include "palindrome.h"

/**
* @brief  Função que verifica se a palavra é um palíndromo.
* @param  word Variável que recebe a palavra que o usuário digitou.
* @param *letters Ponteiro que aponta para um endereço de uma classe do tipo Stack. 
* @param  size Tamanho da palavra.
*/
void palindrome ( string word, Stack <char> *letters, unsigned int size ) {

    string aux_word = "";
    for (unsigned int ii = 0; ii < size; ii++) {

        aux_word += letters->top();
        letters->pop();
    }

    if (word.compare(aux_word) == 0) {
        cout << endl << "The string is a palindrome!" << endl << endl;
    }
    else {
        cout << endl << "The string is not a palindrome!" << endl << endl;
    }
   
}