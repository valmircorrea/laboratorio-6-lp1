/**
* @file	    turma.cpp
* @brief	Arquivo com metodos da classe aluno.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     15/04/2017
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <ostream>
using std::ostream;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <stdlib.h>

#include "turma.h"
#include "aluno.h"
#include "list_dup_enc_sent_.h"

/**
* @brief Contrutor para a classe Turma.
*/
Turma::Turma () {
    codigo = "";
    qtd_alunos = 0;
    media = 0;
}

/**
* @brief  Método que retorna o código da turma.
* @return Retorna o código da turma.
*/
string Turma::getCodigo () {
    return codigo;
}

/**
* @brief  Método que atribui o código de um objeto do tipo Turma.
* @param  n Varável que recebe o código a ser atribuido.
*/
void Turma::setCodigo (string n) {
    codigo = n;
}

/**
* @brief  Método que adiociona um aluno na turma.
* @param  n Varável que recebe o nome do aluno.
* @param  m Variável que recebe a matricula do aluno.
* @param  f Variável que recebe as faltas do aluno.
* @param  nota Variável que recebe a nota do aluno.
* @param  *alunos_cadastrados Ponteiro que recebe a referencia da quantidade de alunos já cadastrados.
*/
void Turma::add_aluno(string n, string m, string f, string nota, int *alunos_cadastrados) {

    Aluno aux;

    aux.setNome (n);
    aux.setMatricula ( atoi ( m.c_str() ) );
    aux.setFaltas ( atoi ( f.c_str() ) );
    aux.setNota (atof (nota.c_str() ) );

    if (alunos.findElement (aux)) {
        cout << "Aluno(a) '" << n << "' já está cadastrado(a)!" << endl;
        return;
    }

    alunos.insertElement(aux);

    qtd_alunos++;
    media += atof( nota.c_str() );
    total_notas += atof (nota.c_str() );
    media = total_notas / qtd_alunos;
    
    *alunos_cadastrados += 1; 
}

/**
 * @details Metodo que remove um aluno da lista de alunos e recalcula a media da turma.
 */
void Turma::Remover_aluno () {

    int m;

    cout << endl << "Digite a matricula do aluno que deseja remover: " << endl;

    cout << "Matricula: ";
    cin >> m;

    Aluno aux;

    aux.setMatricula (m);

    Aluno temp = alunos.findElement2 (aux);

    if (temp.getNome () != " ") {
        string nome = temp.getNome (); 

        total_notas -= temp.getNota ();
        media = total_notas / qtd_alunos;

        alunos.removeElement (temp);

        cout << endl << "Aluno '" << nome << "' removido com sucesso!" << endl;

        qtd_alunos -= 1;
    }
    else {
        cout << "Aluno não cadastrado!" << endl;
    }
}

/** 
* @details  Método que gera para a saída padrão os dados dos alunos.
* @param	saida Arquivo para gravação dos dados.
*/
void Turma::Salvar_arquivo (ofstream &saida) {

        saida << codigo << endl;
        saida << qtd_alunos << endl;
        saida << media << endl;

        saida << alunos.ElementToString () << endl;
}

/** 
 * @details Metodo que carrega os dados de uma turma de um arquivo.
 * @param	arquivo Arquivo para leitura dos dados.
 */
void Turma::Carregar_arquivo (ifstream &arquivo) {

    string temp;

    // Ler o codigo da turma
    getline (arquivo, codigo);

    // Quantidade de alunos
    getline (arquivo, temp);
    int cont = atoi (temp.c_str ());

    // Media das notas
    getline (arquivo, temp);

    int alunos_cadastrados = 0;
    string nome, matricula, faltas, nota;
    
    // Leitura dos alunos
    for (int ii = 0; ii < cont; ii++) {

        getline (arquivo, matricula, ';');
        getline (arquivo, nome, ';');
        getline (arquivo, faltas, ';');
        getline (arquivo, nota, ';');

        getline (arquivo, temp);

        add_aluno (nome, matricula, faltas, nota, &alunos_cadastrados);
    }

    if (alunos_cadastrados > 1) {
        cout << endl << "alunos(as) cadastrados(as) com sucesso!" << endl;
    }
    else if (alunos_cadastrados == 0) {
        cout << endl << "alunos(as) não foram cadastrados(as) com sucesso!" << endl;
    }
    else {
        cout << endl << "aluno(a) '" << nome << "' cadastrado(a) com sucesso!" << endl;        
    }
}

/** 
* @details  Operador "<<" sobrecarregado para gerar os dados dos alunos na saida padrão.
* @param	os Referencia para stream de saida.
* @param	t Referencia para o objeto Turma a ser impresso
* @return	Referencia para stream de saida
*/
ostream& operator << (ostream &os, Turma &t) {

    os << endl << "***************** Turma " << t.codigo << " **********************" << endl;
    os << "Quantidade de alunos: " << t.qtd_alunos << endl << endl;

    if (t.qtd_alunos != 0) {
        os << "Dados dos alunos:" << endl << endl;
        t.alunos.printElements ();
        os << endl << "Media das notas: " << t.media  << endl;
        
    }
    else {
        os << "Não há alunos na turma!" << endl;    
    }
    return os;
}

/** 
* @details Operador "=" sobrecarregado para realizar uma atribuição de um objeto a outro.
* @param	t Referencia para o objeto Turma que invoca o metodo.
* @return	Autoreferência para o objeto que invoca o metodo, com atributos atualizados.
*/
Turma& Turma::operator = (Turma const &t) {

    codigo = t.codigo;
    qtd_alunos = t.qtd_alunos;
    media = t.media;
    
    return *this;
}