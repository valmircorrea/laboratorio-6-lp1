/**
* @file	    aluno.cpp
* @brief	Arquivo com metodos da classe aluno.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     20/04/2017
* @sa       aluno.h
*/

#include "aluno.h"

#include <string>
using std::string;

#include <iostream>
using std::endl;

#include <ostream>
using std::ostream;

#include <sstream>
using std::ostringstream;

/**
* @brief Contrutor para a classe Aluno.
*/
Aluno::Aluno () {
    nome = " ";
    matricula = 0;
    faltas = 0;
    nota = 0;
}

/**
* @brief  Método que retorna a matricula do aluno.
*/
int Aluno::getMatricula () {
    return matricula;
}

/**
* @brief  Método que atribui a matricula do aluno.
* @param  m Variavél que recebe a matricula a ser colocada.
*/
void Aluno::setMatricula (int m) {
    matricula = m;
}

/**
* @brief  Método que retorna o nome do aluno.
*/
string Aluno::getNome () {
    return nome;
}

/**
* @brief  Método que atribui o nome do aluno.
* @param  n Variavél que recebe o nome a ser colocado.
*/
void Aluno::setNome (string n) {
    nome = n;
}

/**
* @brief  Método que retorna as faltas do aluno.
*/
int Aluno::getFaltas () {
    return faltas;
}

/**
* @brief  Método que atribui a quantidade faltas do aluno.
* @param  f Variavél que recebe a quantidade de faltas a ser colocado.
*/
void Aluno::setFaltas (int f) {
    faltas = f;
}

/**
* @brief  Método que retorna a nota do aluno.
*/
int Aluno::getNota () {
    return nota;
}

/**
* @brief  Método que atribui a nota do aluno.
* @param  n Variavél que recebe a nota a ser colocada.
*/
void Aluno::setNota (int n) {
    nota = n;
}

/** 
 * @details Metodo atribui os dados do aluno em uma string para auxiliar na saida dos dados para o arquivo.
 * @return	String com os dados do aluno
 */
string Aluno::dados_para_arquivo () {

    ostringstream oss;

    oss << matricula << ";" << nome << ";" << faltas << ";" << nota << ";" << endl;

    return oss.str ();
}

/** 
* @details  Operador "==" sobrecarregado para comparar dois alunos.
* @param	a Variavél que recebe um objeto Aluno. 
* @return	Verdadeiro ou Falso, dependendo da igualdade entre os dados dos alunos.
*/
bool Aluno::operator== (Aluno const a) {
    
    if ( a.matricula == matricula) {
        return true;
    }

    return false;
}

/** 
* @details  Operador ">" sobrecarregado para verificar a ordenação de cada aluno.
* @param	a Variavél que recebe um objeto Aluno. 
* @return	Verdadeiro ou Falso, dependendo da ordenação e entre os dados dos alunos.
*/
bool Aluno::operator > (Aluno const a) {
    
    if ( a.nome > nome) {
        return true;
    }

    return false;
}

/** 
* @details  Operador "<<" sobrecarregado para gerar os dados dos alunos na saida padrão.
* @param	&os Referência para stream de saida.
* @param	&a Referência para o objeto 'Aluno' a ser impresso.
* @return	Referência para stream de saida.
*/
ostream& operator<< (ostream &os, Aluno &a) {

    os << "Nome: " << a.nome << endl;    
    os << "Matricula: " << a.matricula << endl;
    os << "Faltas: " << a.faltas << endl;
    os << "Nota: " << a.nota << endl;

    return os;
}