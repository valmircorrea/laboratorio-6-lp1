/**
* @file	    funcs_turma.cpp
* @brief	Arquivo com as funções de cadastro de turmas e inclusão de alunos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     15/04/2017
*/

#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;

#include <fstream>
using std:: ifstream;

#include "turma.h"
#include "funcs_turma.h"

/**
* @brief  Função que realiza o cadastro da turma.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  *qtd_turmas Ponteiro que recebe a referência da quantidade de turmas cadastradas.
*/
void cadastrar_turma (Turma *turma, int *qtd_turmas) {

    string leitura;

    cout << "***************** Cadastro da turma *****************" << endl << endl;
    cout << "Digite o código da turma: ";
    cin >> leitura;

    for (int ii = 0; ii < *qtd_turmas; ii++) {
        if (turma[ii].getCodigo () == leitura) {
            
            cout << "Turma já cadastrada!" << endl;

            return;
        }
    }

    int indice_aux = *qtd_turmas;
    *qtd_turmas += 1;
    turma[indice_aux].setCodigo (leitura);

    cout << endl << "turma '" << turma[indice_aux].getCodigo() << "' cadastrada com sucesso!" << endl << endl; 
}

/**
* @brief  Função que realiza o cadastro dos alunos.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  indice_aux Variável que recebe a posição da turma especifica no vetor.
* @param  opcao Variável que recebe a opcao do usuário de cadastrar os alunos pelo prgrama ou por arquivo.
*/
int cadastrar_alunos ( Turma *turma, int indice_aux, int opcao) {
    
    if (indice_aux == -1) {
        return 0;
    }

    if (opcao == 1) {

        string nome, lixo, matricula, faltas, nota, local = "./data/input/alunos_";
        int qtd_alunos;
        local += turma[indice_aux].getCodigo() + ".csv";

        cout << endl << "* ATENÇÃO *" << endl;
        cout << "Certifique que na pasta './data/input' há um arquivo com os dados da turma de alunos com o seguinte nome: ";
        cout << "alunos_" << turma[indice_aux].getCodigo() << endl;     
        cout << endl;

        cout << "Informe a quantidade de alunos: ";
        cin >> qtd_alunos;
        cout << endl;
        
        ifstream entrada (local);

        if (!entrada) {
            cout << "Arquivo não encontrado!" << endl;
            if (qtd_alunos > 1) {
                cout << "alunos(as) não foram cadastrados(as) para a turma '" << turma[indice_aux].getCodigo () << "'" << endl;
            } 
            else {
                cout << "aluno(a) não foi cadastrado(a) para a turma '" << turma[indice_aux].getCodigo () << "'" << endl;            
            }
            return 0;
        }

        int alunos_cadastrados = 0;

        cout << endl;

        getline (entrada, lixo);

        for (int ii = 0; ii < qtd_alunos; ii++) {

            getline (entrada, lixo, '"');
            getline (entrada, nome, '"');

            getline (entrada, lixo, '"');
            getline (entrada, matricula, '"');

            getline (entrada, lixo, '"');
            getline (entrada, nota, '"');

            getline (entrada, lixo, '"');
            getline (entrada, faltas, '"');

            turma[indice_aux].add_aluno(nome, matricula, faltas, nota, &alunos_cadastrados);
        }

        cout << endl;

        if (qtd_alunos > 1 && alunos_cadastrados > 1) {
            cout << "alunos(as) cadastrados(as) com sucesso!" << endl;
        }
        else if (qtd_alunos > 1 && alunos_cadastrados == 0) {
            cout << "alunos(as) não foram cadastrados(as) com sucesso!" << endl;
        }
        else if (alunos_cadastrados == 1) {
            cout << "aluno(a) '" << nome << "' cadastrado(a) com sucesso!" << endl;        
        }
        else {
            cout << "aluno(a) '" << nome << "' não foi cadastrado(a) com sucesso!" << endl;        
        }
        
        entrada.close();
    }
    else {

        int qtd_alunos;

        cout << "Informe a quantidade de alunos: ";
        cin >> qtd_alunos;
        cout << endl;

        cout << endl << "Insira os dados do(s) " << qtd_alunos << " aluno(s)" << endl;

        int alunos_cadastrados = 0;
        string nome;
        for (int ii = 0; ii < qtd_alunos; ii++) {
            cout << endl;
            
            cout << "Nome: ";
            cin >> nome;

            string matricula;
            cout << "Matricula: ";
            cin >> matricula;

            string faltas;
            cout << "Quantidade de faltas: ";
            cin >> faltas;

            string nota;
            cout << "Nota: ";
            cin >> nota;

            turma[indice_aux].add_aluno (nome, matricula, faltas, nota, &alunos_cadastrados);     
        }

        if (qtd_alunos > 1 && alunos_cadastrados > 1) {
            cout << "alunos(as) cadastrados(as) com sucesso!" << endl;
        }
        else if (qtd_alunos > 1 && alunos_cadastrados == 0) {
            cout << "alunos(as) não foram cadastrados(as) com sucesso!" << endl;
        }
        else if (alunos_cadastrados == 1) {
            cout << "aluno(a) '" << nome << "' cadastrado(a) com sucesso!" << endl;        
        }
        else {
            cout << "aluno(a) '" << nome << "' não foi cadastrado(a) com sucesso!" << endl;        
        }        
    }
        
    
    return 0;   
}

/**
* @brief  Função que realiza a busca da turma.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  qtd_turmas Variável que recebe a quantidade de turmas cadastradas.
*/
int busca_turma (Turma *turmas, int qtd_turmas) {
    
    string codigo;
    int indice_turma;

    cout << "Digite o codigo da turma: ";
    cin >> codigo;
    
    for (int ii = 0; ii < qtd_turmas; ii++) {

        if (turmas[ii].getCodigo () == codigo) {
            indice_turma = ii;
            return indice_turma;
        }
    }
    
    cout << endl << "Turma não cadastrada!" << endl;

    return -1;
}

/**
* @brief Função que carrega um arquivo com turmas e seus dados.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  &qtd_turmas Referência a quantidade de turmas cadastradas.
*/
void carregar_arquivo (Turma *turmas, int &qtd_turmas) {

    ifstream entrada ("./data/default/total_turmas");

    if (!entrada) {
        return;
    }

    string temp;

    // lê o numero de turmas
    getline (entrada, temp);
    qtd_turmas = atoi (temp.c_str ());

    for (int ii = 0; ii < qtd_turmas; ii++) {

        turmas[ii].Carregar_arquivo (entrada);
        getline (entrada, temp);
    }

    entrada.close ();
}

/**
* @brief Função que salva em um arquivo as turmas e os dados de seus alunos.
* @param  *turma Ponteiro que aponta para um objeto do tipo Turma.
* @param  qtd_turmas Variável que recebe a quantidade de turmas cadastradas.
* @param  indice_aux Variável que recebe a posição da turma especifica no vetor.
*/
void salvar_turmas (Turma *turmas, int qtd_turmas, int indice_aux) {

    if (indice_aux == -1) {

        ofstream local ("./data/default/total_turmas");

        if (!local) {
        return;
        }

        if (qtd_turmas > 0) {

            local << qtd_turmas << endl;

            for (int ii = 0; ii < qtd_turmas; ii++) {
                if (turmas[ii].getCodigo () != " ") {
                    turmas[ii].Salvar_arquivo (local);
                }
            }
        }

        local.close ();
    }

    else {

        string saida = "./data/output/alunos_";
        saida += turmas[indice_aux].getCodigo() + ".csv";

        ofstream local (saida);

        if (!local) {
        return;
        }

        local << qtd_turmas << endl;

        for (int ii = 0; ii < qtd_turmas; ii++) {
            turmas[ii].Salvar_arquivo (local);
        }

        cout << endl << "Dados da turma '" << turmas[indice_aux].getCodigo() << "' salvos em ./data/output" << endl;

        local.close ();
    }
}