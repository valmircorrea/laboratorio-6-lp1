/**
* @file	    menus.cpp
* @brief	Arquivo com as funções que imprimem os menus do programa.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     15/04/2017
*/

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include "menus.h"

/**
* @brief Função que apresenta o menu principal.
* @return Opcao escolhida pelo usuário.
*/
int menu_principal () {

    int opcao;

    cout << endl;
    cout << "******************* Menu Principal ******************" << endl << endl;
    cout << " 1) Criar turma" << endl;
    cout << " 2) Adicionar alunos a turma" << endl;
    cout << " 3) Listar dados de alunos de uma turma" << endl;
    cout << " 4) Remover aluno de uma turma" << endl;
    cout << " 5) Exportar dados de uma turma para um arquivo" << endl;
    cout << endl;
    cout << " 0) Sair" << endl << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção desejada:" << endl << "-> ";
    cin >> opcao;
    cout << endl;

    return opcao;
}

/**
* @brief Função que apresenta o menu específico para a inclusão de alunos.
* @return Opcao escolhida pelo usuário.
*/
int menu_aluno () {

    int opcao2;

    cout << endl;
    cout << "************* Adicionar alunos a turma **************" << endl << endl;
    cout << " 1) Adicionar aluno por arquivo" << endl;
    cout << " 2) Adicionar aluno pelo programa" << endl << endl;
    cout << endl;
    cout << " 0) Voltar ao menu anterior" << endl << endl;
    cout << "*****************************************************" << endl << endl;            

    cout << "Digite a opção desejada:" << endl << "-> ";
    cin >> opcao2;
    cout << endl;

    return opcao2;
}