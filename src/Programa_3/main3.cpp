/**
* @file	    main3.cpp
* @brief	Arquivo com a função principal.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	15/04/2017
* @date     15/04/2017
*/

#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;

#include "turma.h"
#include "menus.h"
#include "funcs_turma.h"

/**
* @brief Função principal.
*/
int main () {

    Turma p_turma[50];
    int qtd_turmas = 0, opcao, opcao2;

    carregar_arquivo(p_turma, qtd_turmas);

    int indice_aux;

    do {
        opcao = menu_principal ();

        switch (opcao){
            case 1:
                cadastrar_turma (p_turma, &qtd_turmas);
                break;
            case 2:
                opcao2 = menu_aluno ();
                if (opcao2 == 0) {
                    break;
                }
                indice_aux = busca_turma (p_turma, qtd_turmas);
                if (indice_aux != -1) {
                    cadastrar_alunos (p_turma, indice_aux, opcao2);
                }
                break;
            case 3:
                indice_aux = busca_turma (p_turma, qtd_turmas);

                if (indice_aux != -1) {

                    cout << p_turma[indice_aux];
                }
                else {
                    cout << endl << "Não há turmas cadastradas!" << endl;
                }
                break;
            case 4:
                indice_aux = busca_turma (p_turma, qtd_turmas);

                if (qtd_turmas != 0) {

                    cout << p_turma[indice_aux];

                    p_turma[indice_aux].Remover_aluno();
                }
                else {
                    cout << "Não há turmas cadastradas!" << endl;
                }
                break;
            case 5:
                indice_aux = busca_turma (p_turma, qtd_turmas);
                salvar_turmas(p_turma, qtd_turmas, indice_aux);
                break;
            default:
                
                system("rm -rf ./data/default/*");
                cout << "Programa encerrado" << endl;
                cout << "Created by Valmir Correa" << endl;
        }

    } while (opcao != 0);

    if (qtd_turmas > 0) {

            indice_aux = -1;
        salvar_turmas(p_turma, qtd_turmas, -1);
    }
    
    return 0;
}